#! /usr/bin/env python

"""

Implementation of the perceptron learning algorithm (PLA)
as explained in the 1st lecture of CalTech Machine Learning Mooc

"""

import random


def normalize(vector):
    from math import sqrt
    magnitude = sqrt(sum([x**2 for x in vector]))
    return [x/magnitude for x in vector]


def dot_product(v1, v2):
    v1 = normalize(v1)
    v2 = normalize(v2)
    value = sum([x*y for x,y in zip(v1,v2)])
    return int(value/abs(value))


def add_vectors(v1, v2):
    v3 = [x+y for x,y in zip(v1,v2)]
    return v3


def scalar_mult(s, v1):
    v2 = [s*x for x in v1]
    return v2


def main():

    import random

    ##### Data setup #####
    nsims = 1000
    simulated_data = []
    for i in range(nsims):
        # the artificial coordinate s.t. w0*x0 = w0
        x0 = 1
        x1 = random.uniform(-1,1)
        x2 = random.uniform(-1,1)
        simulated_data.append((x0, x1, x2))

    # w0 is our threshold
    # i.e. w . x > w0 approved and vice versa
    w0 = random.uniform(-1,1)
    w1 = random.uniform(-1,1)
    w2 = random.uniform(-1,1)
    target = (w0, w1, w2)

    classified_data = []
    for val in simulated_data:
        dp = dot_product(val, target)
        classified_data.append(dp)

    # hypothesis weights for g
    tw0 = random.uniform(-1,1)
    tw1 = random.uniform(-1,1)
    tw2 = random.uniform(-1,1)
    test = (tw0, tw1, tw2)

    changed = False
    for i in range(10000):
        print("Iteration %s" % i)
        print(target, test)
        print(normalize(test))
        changed = False

        for x in range(len(simulated_data)):
            dp = dot_product(simulated_data[x], test)
            if dp != classified_data[x]:
                addend = scalar_mult(classified_data[x], simulated_data[x])
                test = add_vectors(test, addend)
                changed = True
                break

        if not(changed):
            print("optimized!")
            break

    print("generating probability of difference!")
    good = 0
    bad = 0
    weird = 0
    for x in range(1000):
        x0 = 1
        x1 = random.uniform(-1,1)
        x2 = random.uniform(-1,1)
        test_value = (x0, x1, x2)

        target_dp = dot_product(test_value, target)
        hypothesis_dp = dot_product(test_value, test)

        if target_dp == hypothesis_dp:
            good += 1
        elif target_dp != hypothesis_dp:
            bad += 1
        else:
            weird += 1

    print(good, bad, weird, bad/(good+bad+weird))

    return 0


if __name__ == '__main__':
    main()
